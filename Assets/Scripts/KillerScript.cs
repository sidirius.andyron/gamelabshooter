﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerScript : MonoBehaviour
{
    #region Unity Callbacks

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerScript>();
        if (player == null) return;
        player.IsDead = true;
        PlayerManager.Singleton.RecordScore();
        GameManager.Singleton.CheckEnd();
    }

    #endregion

}
