﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorShooter : MonoBehaviour
{
    #region Internal Definitions

    public enum ShotTypesEnum
    {
        Ice,
        Dirt,
    }
    public const int ClipSize = 5;

    #endregion

    #region Members

    public PlayerScript Player;
    public float ReloadDuration = 2f;
    public float ShootForce;
    public Transform Guntip;
    public Projectile IceProjectile, DirtProjectile;
    private Queue<ShotTypesEnum> Clip = new Queue<ShotTypesEnum>();
    private float ReloadTime = 0f;
    private Coroutine ReloadingCoroutine;


    #endregion

    #region Unity Callbacks

    private void Awake()
    {

    }

    #endregion

    #region Actions

    public void Init()
    {
        for (int i = 0; i < ClipSize; i++)
        {
            LoadABullet();
        }
    }

    public void LoadABullet()
    {
        int type = Random.Range(0, 2);
        if (type == 0)
        {
            Clip.Enqueue(ShotTypesEnum.Dirt);
            PlayerMasterUI.Singleton.AddBullet(Player.ID, ShotTypesEnum.Dirt);
            Debug.Log("Loaded Dirt shot");
        }
        else
        {
            Clip.Enqueue(ShotTypesEnum.Ice);
            PlayerMasterUI.Singleton.AddBullet(Player.ID, ShotTypesEnum.Ice);
            Debug.Log("Loaded Ice shot");
        }
    }

    public void Shoot(Vector3 shootDirection, float zAngle)
    {
        if (Clip.Count > 0)
        {
            var type = Clip.Dequeue();
            Quaternion rot = Quaternion.Euler(0f, 0f, zAngle);
            if (type == ShotTypesEnum.Dirt)
            {
                var projectile = Instantiate(DirtProjectile, Guntip.position, rot);
                projectile.ShootingDirection = shootDirection * ShootForce;
                PlayerMasterUI.Singleton.ShootBullet(Player.ID);
            }
            else if (type == ShotTypesEnum.Ice)
            {
                var projectile = Instantiate(IceProjectile, Guntip.position, rot);
                projectile.ShootingDirection = shootDirection * ShootForce;
                PlayerMasterUI.Singleton.ShootBullet(Player.ID);
            }

            if (ReloadingCoroutine == null)
                ReloadingCoroutine = StartCoroutine(Reload());
        }
        else
        {
            if (ReloadingCoroutine == null)
                ReloadingCoroutine = StartCoroutine(Reload());
        }
    }

    #endregion

    #region Coroutines

    private IEnumerator Reload()
    {
        ReloadTime = 0f;
        while (Clip.Count < ClipSize)
        {
            yield return null;
            ReloadTime += Time.deltaTime;
            if (ReloadTime >= ReloadDuration)
            {
                LoadABullet();
                ReloadTime = 0f;
            }
        }
        ReloadingCoroutine = null;
    }

    #endregion

}
