﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public Vector3 Speed = Vector3.up;

    void Update()
    {
        if (!GameManager.Singleton.InGame || GameManager.Singleton.Paused) return;
        transform.position = transform.position + (Speed * Time.deltaTime);
    }
}
