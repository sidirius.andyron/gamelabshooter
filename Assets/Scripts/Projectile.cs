﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    #region Internal Definitions

    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
    }

    public const string DirtTag = "Dirt";
    public const string IceTag = "Ice";

    #endregion

    #region Members

    public Vector2 GridDirection = new Vector2(1f, 0.8f);
    public ColorShooter.ShotTypesEnum ColorType;
    public Vector3 Gravity = new Vector3(0f, -5f, 0f);
    public Vector3 ShootingDirection;
    #endregion

    #region Unity Callbacks

    private void FixedUpdate()
    {
        ShootingDirection += Gravity * Time.deltaTime;
        transform.position = transform.position + (ShootingDirection * Time.deltaTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        var box = other as BoxCollider;
        if (box == null) return;
        Vector3 direction = (transform.position - (other.transform.position + box.center)).normalized;
        direction.z = 0f;

        if (ColorType == ColorShooter.ShotTypesEnum.Dirt)
        {
            if (string.Compare(other.tag, DirtTag, true) == 0)
            {
                GameManager.Singleton.DynamicBlocks.Remove(other.transform);
                Destroy(other.gameObject);
            }
            else if (string.Compare(other.tag, IceTag, true) == 0 || (string.Compare(other.tag, "Universal", true) == 0))
            {
                if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y))
                {
                    if (direction.y > 0)
                        SpawnBlock(Direction.Up, ColorType, other.transform);
                    else
                        SpawnBlock(Direction.Down, ColorType, other.transform);
                }
                else
                {
                    if (direction.x > 0)
                        SpawnBlock(Direction.Right, ColorType, other.transform);
                    else
                        SpawnBlock(Direction.Left, ColorType, other.transform);
                }
            }
        }
        else if (ColorType == ColorShooter.ShotTypesEnum.Ice)
        {
            if (string.Compare(other.tag, DirtTag, true) == 0 || (string.Compare(other.tag, "Universal", true) == 0))
            {
                if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y))
                {
                    if (direction.y > 0)
                        SpawnBlock(Direction.Up, ColorType, other.transform);
                    else
                        SpawnBlock(Direction.Down, ColorType, other.transform);
                }
                else
                {
                    if (direction.x > 0)
                        SpawnBlock(Direction.Right, ColorType, other.transform);
                    else
                        SpawnBlock(Direction.Left, ColorType, other.transform);
                }
            }
            else if (string.Compare(other.tag, IceTag, true) == 0)
            {
                GameManager.Singleton.DynamicBlocks.Remove(other.transform);
                Destroy(other.gameObject);
            }
        }
        Destroy(gameObject);
    }

    #endregion

    #region Helpers

    public void SpawnBlock(Direction dir, ColorShooter.ShotTypesEnum type, Transform originalBlock)
    {
        Vector3 nuPos = originalBlock.position;
        switch (dir)
        {
            case Direction.Up:
                nuPos.y += GridDirection.y;
                break;
            case Direction.Down:
                nuPos.y -= GridDirection.y;
                break;
            case Direction.Left:
                nuPos.x -= GridDirection.x;
                break;
            case Direction.Right:
                nuPos.x += GridDirection.x;
                break;
        }
        if (type == ColorShooter.ShotTypesEnum.Dirt)
        {
            var block = Instantiate(GameManager.Singleton.DirtBlock, nuPos, Quaternion.identity, GameManager.Singleton.DynamicParent);
            GameManager.Singleton.DynamicBlocks.Add(block);
        }
        else if (type == ColorShooter.ShotTypesEnum.Ice)
        {
            var block = Instantiate(GameManager.Singleton.IceBlock, nuPos, Quaternion.identity, GameManager.Singleton.DynamicParent);
            GameManager.Singleton.DynamicBlocks.Add(block);
        }

    }

    #endregion
}
