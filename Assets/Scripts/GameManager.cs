﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Members

    private static GameManager singleton;
    public static GameManager Singleton
    {
        get
        {
            return singleton;
        }
        private set
        {
            singleton = value;
        }
    }
    public Transform DirtBlock, IceBlock;
    public Transform DynamicParent;
    public Transform[] LevelList;
    private Transform CurrentLevel = null;
    public bool InGame = false;
    public bool Paused = false;
    public List<Transform> DynamicBlocks = new List<Transform>();

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        Singleton = this;
    }

    #endregion

    #region Actions

    public void CheckEnd()
    {
        int live = 0;
        for (int i = 0; i < PlayerManager.Singleton.Players.Count; i++)
        {
            if (!PlayerManager.Singleton.Players[i].IsDead)
                live++;
        }
        if(live < 2)
        {
            StartCoroutine(TimedRestart());
        }
    }

    public void RespawnLevel()
    {
        if(CurrentLevel != null)
        {
            Destroy(CurrentLevel.gameObject);
        }
        for (int i = 0; i < DynamicBlocks.Count; i++)
        {
            Destroy(DynamicBlocks[i].gameObject);
        }
        DynamicBlocks.Clear();
        var cam = Camera.main;
        cam.transform.position = new Vector3(0f, -1f, -11f);
        var ind = Random.Range(0, LevelList.Length);
        CurrentLevel = Instantiate(LevelList[ind], Vector3.zero, Quaternion.identity);
        InGame = true;
    }

    #endregion

    #region Coroutines

    public IEnumerator TimedRestart()
    {
        yield return new WaitForSeconds(2f);
        RespawnLevel();
        PlayerManager.Singleton.RespawnPlayers();
    }

    internal void PauseGame()
    {
        Paused = true;
        PauseMenuUI.Singleton.Show(true);
    }

    #endregion
}
