﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : MonoBehaviour
{
    #region Members

    public GameLobbyUI LobbyScript;
    public CanvasGroup UI;

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        TurnOn(true);
    }

    #endregion

    #region Actions

    public void TurnOn(bool on)
    {
        if (on)
        {
            UI.alpha = 1f;
            UI.blocksRaycasts = true;
            UI.interactable = true;
        }
        else
        {
            UI.alpha = 0f;
            UI.blocksRaycasts = false;
            UI.interactable = false;
        }
    }

    #endregion

    #region Callbacks

    public void StartGame()
    {
        TurnOn(false);
        LobbyScript.TurnOn(true);
    }

    public void EndGame()
    {
        Application.Quit();
    }

    #endregion
}
