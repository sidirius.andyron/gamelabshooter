﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLobbyUI : MonoBehaviour
{
    #region Members

    public MainMenuUI MenuScript;
    public CanvasGroup UI;
    public TMPro.TMP_Text[] Lines;

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        TurnOn(false);
    }

    private void Start()
    {
        for (int i = 0; i < PlayerManager.Singleton.PlayerInputPairs.Count; i++)
        {
            PlayerManager.Singleton.PlayerInputPairs[i].UpdateUI += UpdateUI;
            PlayerManager.Singleton.PlayerInputPairs[i].UpdateUIText();
        }
    }

    #endregion

    #region Actions

    public void TurnOn(bool on)
    {
        if (on)
        {
            UI.alpha = 1f;
            UI.blocksRaycasts = true;
            UI.interactable = true;
        }
        else
        {
            UI.alpha = 0f;
            UI.blocksRaycasts = false;
            UI.interactable = false;
        }
    }

    #endregion

    #region Callbacks

    public void StartGame()
    {
        TurnOn(false);
        GameManager.Singleton.RespawnLevel();
        PlayerManager.Singleton.SpawnPlayers();
    }

    public void IncPlayerAtIndex(int playerInd)
    {
        PlayerManager.Singleton.IncInputs(playerInd);
    }

    public void DecPlayerAtIndex(int playerInd)
    {
        PlayerManager.Singleton.DecInputs(playerInd);
    }

    public void BackClicked()
    {
        TurnOn(false);
        MenuScript.TurnOn(true);
    }

    private void UpdateUI(int ind, string text)
    {
        Lines[ind].text = text;
    }

    #endregion
}
