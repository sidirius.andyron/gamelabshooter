﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuUI : MonoBehaviour
{
    public static PauseMenuUI Singleton
    {
        get;
        private set;
    }
    public CanvasGroup Canvas;

    public void Awake()
    {
        Singleton = this;
        Show(false);
    }

    public void Show(bool show)
    {
        if (show)
        {
            Canvas.alpha = 1f;
            Canvas.blocksRaycasts = true;
            Canvas.interactable = true;
        }
        else
        {
            Canvas.alpha = 0f;
            Canvas.blocksRaycasts = false;
            Canvas.interactable = false;
        }
    }

    public void UnPause()
    {
        Show(false);
        GameManager.Singleton.Paused = false;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
