﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMasterUI : MonoBehaviour
{
    #region Members

    private static PlayerMasterUI singleton;
    public static PlayerMasterUI Singleton
    {
        get { return singleton; }
        set
        {
            singleton = value;
        }
    }

    public HorizontalLayoutGroup[] ClipUI;
    public List<RectTransform>[] ClipInstances;
    public TMPro.TMP_Text[] PlayerScore;
    public RectTransform IceBulletUI, DirtBulletUI;

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        Singleton = this;
        ClipInstances = new List<RectTransform>[4];
        for (int i = 0; i < 4; i++)
        {
            ClipInstances[i] = new List<RectTransform>();
        }
    }

    #endregion

    #region Actions

    public void AddBullet(int ind, ColorShooter.ShotTypesEnum type)
    {
        switch (type)
        {
            case ColorShooter.ShotTypesEnum.Ice:
                var ice = Instantiate(IceBulletUI, ClipUI[ind].transform);
                ClipInstances[ind].Add(ice);
                break;
            case ColorShooter.ShotTypesEnum.Dirt:
                var dirt = Instantiate(DirtBulletUI, ClipUI[ind].transform);
                ClipInstances[ind].Add(dirt);
                break;
        }
    }

    public void ShootBullet(int ind)
    {
        Destroy(ClipInstances[ind][0].gameObject);
        ClipInstances[ind].RemoveAt(0);
    }

    public void UpdateScore()
    {
        for (int i = 0; i < PlayerManager.Singleton.Players.Count; i++)
        {
            PlayerScore[i].text = $"Score: {PlayerManager.Singleton.Players[i].Score}";
        }
    }


    #endregion
}
