﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    #region Internal Definitions

    public enum DeviceTypeEnum
    {
        KeyboardMouse,
        Gamepad,
    }

    public class PossibleDevice
    {
        public InputDevice Device;
        public bool Active;

        public PossibleDevice(InputDevice device)
        {
            Device = device;
            Active = true;
        }
    }

    #endregion

    #region Members

    private static InputManager singleton;
    public static InputManager Singleton
    {
        get { return singleton; }
        private set
        {
            if (singleton == null)
                singleton = value;
        }
    }

    public List<PossibleDevice> GamepadDevices;
    public List<PossibleDevice> KeyboardDevices;

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        GamepadDevices = new List<PossibleDevice>();
        KeyboardDevices = new List<PossibleDevice>();
        Singleton = this;
        InputSystem.onDeviceChange += DeviceChange;
        for (int i = 0; i < InputSystem.devices.Count; i++)
        {
            AddDevice(InputSystem.devices[i]);
        }
    }

    private void DeviceChange(InputDevice device, InputDeviceChange eventType)
    {
        Debug.Log($"Device { device.displayName } fired { eventType }");
        switch (eventType)
        {
            case InputDeviceChange.Added:
                AddDevice(device);
                break;
            case InputDeviceChange.Removed:
            case InputDeviceChange.Disconnected:
                DisconnectDevice(device);
                break;
        }
    }

    #endregion

    #region Callbacks



    #endregion

    #region Helpers

    private bool AddDevice(InputDevice device)
    {
        if (device is Keyboard)//string.Compare(device.description.deviceClass, "keyboard", true) == 0)
        {
            if (DeviceListContains(KeyboardDevices, device) != null)
                return false;
            KeyboardDevices.Add(new PossibleDevice(device));
            Debug.Log("Added Keyboard.");
            return true;
        }
        else if (device is Gamepad || device is Joystick) //string.Compare(device.description.deviceClass, "gamepad", true) == 0)
        {
            if (DeviceListContains(GamepadDevices, device) != null)
                return false;
            GamepadDevices.Add(new PossibleDevice(device));
            Debug.Log("Added Gamepad.");
            return true;
        }
        return false;
    }

    private void DisconnectDevice(InputDevice device)
    {
        PossibleDevice item;
        if (string.Compare(device.description.deviceClass, "keyboard", true) == 0)
        {
            if ((item = DeviceListContains(KeyboardDevices, device)) != null)
            {
                item.Active = false;
            }

        }
        else if (string.Compare(device.description.deviceClass, "gamepad", true) == 0)
        {
            if ((item = DeviceListContains(GamepadDevices, device)) != null)
            {
                item.Active = false;
            }
        }
    }

    private PossibleDevice DeviceListContains(List<PossibleDevice> list, InputDevice device)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].Device == device)
                return list[i];
        }
        return null;
    }

    #endregion
}
