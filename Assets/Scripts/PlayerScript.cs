﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

public class PlayerScript : MonoBehaviour
{
    #region Members

    public CharacterController Character;
    public MeshRenderer Renderer;
    public ColorShooter Shooter;
    public float Speed = 5f;
    public float GravityBar = -10f;
    public float Gravity = 10f;
    public float JumpHeight = 4f;
    public Transform AimingArrow;
    public float AimingAngle = 0f;
    public bool IsDead = false;
    public int Score = 0;
    public int ID;
    private bool FacingRight = true;
    private bool IsJumping = false;
    private bool IsAiming = false;
    private Vector3 TakeOffPoint;
    public Vector3 AimVector;
    private Vector3 MoveVector;
    private Vector3 GravityVec = new Vector3(0f, -10f, 0f);

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        GravityVec = Vector3.down * Gravity;
    }

    private void FixedUpdate()
    {
        if (IsDead || GameManager.Singleton.Paused) return;
        AimingArrow.localRotation = Quaternion.Euler(AimingAngle, -90f, 0f);
        if ((Character.collisionFlags & CollisionFlags.Above) > 0)
        {
            IsJumping = false;
            MoveVector.y = 0f;
        }
        if (IsJumping)
        {
            if ((transform.position - TakeOffPoint).magnitude > (JumpHeight / 2f))
            {
                IsJumping = false;
                MoveVector += GravityVec * Time.deltaTime;
            }
        }
        else
        {
            MoveVector += GravityVec * Time.deltaTime;
        }

        if (MoveVector.y < GravityBar)
            MoveVector.y = GravityBar;
        if (Character.isGrounded && MoveVector.y < -2f)
            MoveVector.y = -2f;
        Character.Move(MoveVector * Time.deltaTime);
    }

    #endregion

    #region Callbacks

    public void OnJumpClick(CallbackContext context)
    {
        if (IsDead || GameManager.Singleton.Paused) return;
        var jump = context.ReadValue<float>();
        if (jump > 0.5f)
        {
            if (!Character.isGrounded) return;
            IsJumping = true;
            TakeOffPoint = transform.position;
            MoveVector.y = Mathf.Sqrt(JumpHeight * Gravity);
        }
        else
        {
            IsJumping = false;
        }
    }

    public void OnMove(CallbackContext context)
    {
        if (IsDead || GameManager.Singleton.Paused) return;
        var move = context.ReadValue<Vector2>();

        if (move.magnitude > 0.25f)
        {
            AimVector = move;
            AimVector.Normalize();
            AimingAngle = Vector2.Angle(AimVector, Vector2.left);
            AimingAngle = AimVector.y > 0 ? -AimingAngle : AimingAngle;
        }
        FacingRight = move.x > 0;
        MoveVector.x = move.x * Speed;
    }

    public void Aiming(CallbackContext context)
    {
        if (IsDead || GameManager.Singleton.Paused) return;

        var aiming = context.ReadValue<float>();
        IsAiming = aiming > 0.5f;
        if (!IsAiming)
        {
            if (Character.isGrounded && AimVector.y < 0) return;
            Shooter.Shoot(AimVector, AimingAngle);
        }
    }

    public void PauseGame()
    {
        GameManager.Singleton.PauseGame();
    }

    #endregion
}
