﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerManager : MonoBehaviour
{
    #region Internal Definitions

    public enum SelectedInput
    {
        Empty,
        Keyboard,
        Gamepad
    }

    public const int MaxPlayers = 4;

    public class PlayerPair
    {
        public SelectedInput InputType;
        public int InputIndex = 0;
        public int Id;
        public bool IsActive
        {
            get
            {
                switch (InputType)
                {
                    case SelectedInput.Keyboard:
                        return InputManager.Singleton.KeyboardDevices[0].Active;
                    case SelectedInput.Gamepad:
                        return InputManager.Singleton.GamepadDevices[InputIndex].Active;
                }
                return true;
            }
        }
        public event System.Action<int, string> UpdateUI;

        public PlayerPair(SelectedInput type, int id)
        {
            InputType = type;
            Id = id;
        }

        public void IncInput()
        {
            switch (InputType)
            {
                case SelectedInput.Empty:
                    if (InputManager.Singleton.KeyboardDevices.Count > 0)
                    {
                        InputType = SelectedInput.Keyboard;
                        InputIndex = 0;
                    }
                    else if (InputManager.Singleton.GamepadDevices.Count > 0)
                    {
                        InputType = SelectedInput.Gamepad;
                        InputIndex = 0;
                    }
                    break;
                case SelectedInput.Keyboard:
                    InputIndex++;
                    if (InputIndex >= Singleton.KeyboardScheme.Length)
                    {
                        if (InputManager.Singleton.GamepadDevices.Count > 0)
                        {
                            InputType = SelectedInput.Gamepad;
                            InputIndex = 0;
                        }
                        else
                        {
                            InputType = SelectedInput.Empty;
                            InputIndex = 0;
                        }
                    }
                    break;
                case SelectedInput.Gamepad:
                    InputIndex++;
                    if (InputIndex >= InputManager.Singleton.GamepadDevices.Count)
                    {
                        InputType = SelectedInput.Empty;
                        InputIndex = 0;
                    }
                    break;
            }
            if (!IsActive)
                IncInput();
            else
                UpdateUIText();
        }

        public void DecInput()
        {
            switch (InputType)
            {
                case SelectedInput.Empty:
                    if (InputManager.Singleton.GamepadDevices.Count > 0)
                    {
                        InputType = SelectedInput.Gamepad;
                        InputIndex = InputManager.Singleton.GamepadDevices.Count - 1;
                    }
                    else if (InputManager.Singleton.KeyboardDevices.Count > 0)
                    {
                        InputType = SelectedInput.Keyboard;
                        InputIndex = Singleton.KeyboardScheme.Length - 1;
                    }
                    break;
                case SelectedInput.Keyboard:
                    InputIndex--;
                    if (InputIndex < 0)
                    {
                        InputType = SelectedInput.Empty;
                        InputIndex = 0;
                    }
                    break;
                case SelectedInput.Gamepad:
                    InputIndex--;
                    if (InputIndex < 0)
                    {
                        if (InputManager.Singleton.KeyboardDevices.Count > 0)
                        {
                            InputType = SelectedInput.Keyboard;
                            InputIndex = Singleton.KeyboardScheme.Length - 1;
                        }
                        else
                        {
                            InputType = SelectedInput.Empty;
                            InputIndex = 0;
                        }
                    }
                    break;
            }
            if (!IsActive)
                DecInput();
            else
                UpdateUIText();
        }

        public void UpdateUIText()
        {
            switch (InputType)
            {
                case SelectedInput.Empty:
                    UpdateUI?.Invoke(Id, "<NONE>");
                    break;
                case SelectedInput.Keyboard:
                        UpdateUI?.Invoke(Id, $"Keyboard { InputIndex + 1}");
                    break;
                case SelectedInput.Gamepad:
                    UpdateUI?.Invoke(Id, $"Gamepad { InputIndex + 1}");
                    break;
            }
        }
    }

    #endregion

    #region Members

    private static PlayerManager singleton;
    public static PlayerManager Singleton
    {
        get { return singleton; }
        private set
        {
            singleton = value;
        }
    }

    public PlayerInput PlayerPrefab;
    public List<PlayerScript> Players = new List<PlayerScript>();
    public string[] KeyboardScheme;
    public string GamepadScheme = "";
    public List<PlayerPair> PlayerInputPairs = new List<PlayerPair>();
    public List<Transform> SpawnPoints = new List<Transform>();
    public Material[] PlayerMaterials;

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        Singleton = this;
        for (int i = 0; i < MaxPlayers; i++)
        {
            PlayerInputPairs.Add(new PlayerPair(SelectedInput.Empty, i));
        }
    }

    #endregion

    #region Helpers

    public void IncInputs(int playerInd)
    {
        PlayerInputPairs[playerInd].IncInput();
    }

    public void DecInputs(int playerInd)
    {
        PlayerInputPairs[playerInd].DecInput();
    }

    public void SpawnPlayers()
    {
        for (int i = 0; i < PlayerInputPairs.Count; i++)
        {
            if (PlayerInputPairs[i].InputType != SelectedInput.Empty)
            {
                var deviceList = GetLinkedDevices(PlayerInputPairs[i]);
                var playerInput = PlayerInput.Instantiate(PlayerPrefab.gameObject, playerIndex: i, pairWithDevices: deviceList);
                playerInput.SwitchCurrentActionMap(KeyboardScheme[PlayerInputPairs[i].InputIndex]);
                var script = playerInput.GetComponent<PlayerScript>();
                script.transform.position = SpawnPoints[i].position;
                script.Character.enabled = true;
                script.ID = i;
                script.Renderer.material = PlayerMaterials[i];
                script.Shooter.Init();
                Players.Add(script);
            }
        }
    }

    public void RespawnPlayers()
    {
        for (int i = 0; i < Players.Count; i++)
        {
            Players[i].Character.enabled = false;
            Players[i].transform.position = SpawnPoints[i].position;
            Players[i].Character.enabled = true;
            Players[i].IsDead = false;
        }
    }

    private InputDevice[] GetLinkedDevices(PlayerPair pair)
    {
        var list = new List<InputDevice>();
        switch (pair.InputType)
        {
            case SelectedInput.Keyboard:
                for (int i = 0; i < InputManager.Singleton.KeyboardDevices.Count; i++)
                {
                    list.Add(InputManager.Singleton.KeyboardDevices[i].Device);
                }
                break;
            case SelectedInput.Gamepad:
                list.Add(InputManager.Singleton.GamepadDevices[pair.InputIndex].Device);
                break;
        }
        return list.ToArray();
    }

    private string GetInputScheme(PlayerPair pair)
    {
        switch (pair.InputType)
        {
            case SelectedInput.Empty:
                break;
            case SelectedInput.Keyboard:
                return KeyboardScheme[pair.InputIndex];
            case SelectedInput.Gamepad:
                return GamepadScheme;
        }
        return string.Empty;
    }

    public void RecordScore()
    {
        for (int i = 0; i < Players.Count; i++)
        {
            if (!Players[i].IsDead)
            {
                Players[i].Score++;
            }
        }
        PlayerMasterUI.Singleton.UpdateScore();
    }

    #endregion
}
